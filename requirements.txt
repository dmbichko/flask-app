flask==2.0.1
requests==2.25.1
flake8==3.9.2
pycodestyle==2.7.0
tox==3.23.1
